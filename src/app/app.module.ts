import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatButtonModule } from '@angular/material/button';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { MatIconModule } from '@angular/material/icon';
import { TranslateModule } from '@ngx-translate/core';
import 'hammerjs';

import { FuseModule } from '@fuse/fuse.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseProgressBarModule, FuseSidebarModule, FuseThemeOptionsModule } from '@fuse/components';

import { fuseConfig } from 'app/fuse-config';

import { FakeDbService } from 'app/fake-db/fake-db.service';
import { AppComponent } from 'app/app.component';
import { LayoutModule } from 'app/layout/layout.module';
import { SampleModule } from 'app/main/sample/sample.module';
import { LoginModule } from './main/auth/login/login.module';
import { ForgotPasswordModule } from 'app/main/auth/forgot-password/forgot-password.module';
import { DashboardModule } from 'app/main/dashboard/dashboard.module';
import { ClientModule } from './main/client_/client.module';
import { HeroModule } from './main/hero/hero.module';
import { NotificationComponent } from './main/notification/notification.component';
import { CalendarModule } from './main/calendar/calendar.module';
import { TodoModule } from './main/todo/todo.module';
import { ChangePasswordModule } from './main/auth/change-password/change-password.module'
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { ContentModule } from './main/content/content.module';
import { InvoiceModule } from './main/invoice/invoice.module';
import { CsmClientModule } from './main/csm-clients/csm-clients.module';

const appRoutes: Routes = [
    {
        path      : '**',
        redirectTo: localStorage.getItem('authorization') === null ? 'auth/login' : 'dashboard'
    }
];

@NgModule({
    declarations: [
        AppComponent,
        NotificationComponent
    ],
    imports     : [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        RouterModule.forRoot(appRoutes),

        TranslateModule.forRoot(),
        InMemoryWebApiModule.forRoot(FakeDbService, {
            delay             : 0,
            passThruUnknownUrl: true
        }),

        // Material moment date module
        MatMomentDateModule,

        // Material
        MatButtonModule,
        MatIconModule,

        // Fuse modules
        FuseModule.forRoot(fuseConfig),
        FuseProgressBarModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseThemeOptionsModule,

        // App modules
        LayoutModule,
        SampleModule,
        LoginModule,
        ForgotPasswordModule,
        DashboardModule,
        ClientModule,
        HeroModule,
        CalendarModule,
        TodoModule,
        ChangePasswordModule,
        NgxMatSelectSearchModule,
        ContentModule,
        InvoiceModule,
        CsmClientModule
    ],
    bootstrap   : [
        AppComponent
    ]
})
export class AppModule
{
}
