import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, takeUntil } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';
import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';

import { ClientService } from 'app/main/client_/client.service';
import { MatTableDataSource } from '@angular/material';

@Component({
    selector     : 'invoice-list',
    templateUrl  : './invoice.component.html',
    styleUrls    : ['./invoice.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class InvoiceListComponent implements OnInit, OnDestroy
{
    dialogRef: any;
    hasSelectedClient: boolean;
    searchInput: FormControl;
    
    // Private
    private _unsubscribeAll: Subject<any>;
    /**
     * Constructor
     *
     * @param {ClientService} _clientService
     * @param {FuseSidebarService} _fuseSidebarService
     * @param {MatDialog} _matDialog
     */
    constructor(
        private _clientService: ClientService,
        private _fuseSidebarService: FuseSidebarService,
        private _matDialog: MatDialog
    )
    {
        // Set the defaults
        this.searchInput = new FormControl('');

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    displayedColumns: string[] = [
        'client',
        'startday',
        'content',
        'quantity',
        'jan2019',
        'feb2019',
        'mar2019',
        'apr2019',
        'may2019'
    ];
    
    dataSource = new MatTableDataSource<TableData>(TABLE_DATA);
    isSticky (column: string): boolean {
        return column === 'client' || column === 'startday' || column === 'content' || column === 'quantity' ? true : false;
    }
    
    ngOnInit(): void
    {
        this._clientService.onSelectedClientsChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(selectClient => {
                this.hasSelectedClient = selectClient.length > 0;
            });

        this.searchInput.valueChanges
            .pipe(
                takeUntil(this._unsubscribeAll),
                debounceTime(300),
                distinctUntilChanged()
            )
            .subscribe(searchText => {
                this._clientService.onSearchTextChanged.next(searchText);
            });
    }

    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    toggleSidebar(name): void
    {
        this._fuseSidebarService.getSidebar(name).toggleOpen();
    }
}

export interface TableData {
    client : string;
    startday : string;
    content : string;
    quantity : string;
    jan2019 : string;
    feb2019 : string;
    mar2019 : string;
    apr2019 : string;
    may2019 : string;
    jun2019 : string;
    jul2019 : string;
    aug2019 : string;
    sep2019 : string;
    oct2019 : string;
    nov2019 : string;
    dec2019 : string;
  }
  
  const TABLE_DATA: TableData[] = [
    {
        client : 'This is Company A',
        startday : '$100',
        content : '',
        quantity : '$320',
        jan2019 : '',
        feb2019 : '',
        mar2019 : '',
        apr2019 : '',
        may2019 : '',
        jun2019 : '',
        jul2019 : '',
        aug2019 : '',
        sep2019 : '',
        oct2019 : '',
        nov2019 : '',
        dec2019 : ''
    },
    {
        client : 'This is Company B',
        startday : '$340',
        content : '$200',
        quantity : '',
        jan2019 : '',
        feb2019 : '',
        mar2019 : '',
        apr2019 : '',
        may2019 : '',
        jun2019 : '',
        jul2019 : '',
        aug2019 : '',
        sep2019 : '',
        oct2019 : '',
        nov2019 : '',
        dec2019 : ''
    },
    {
        client : 'This is Company C',
        startday : '',
        content : '',
        quantity : '$320',
        jan2019 : '',
        feb2019 : '',
        mar2019 : '',
        apr2019 : '',
        may2019 : '',
        jun2019 : '',
        jul2019 : '',
        aug2019 : '',
        sep2019 : '',
        oct2019 : '',
        nov2019 : '',
        dec2019 : ''
    },

    {
        client : 'This is Company D',
        startday : '$400',
        content : '',
        quantity : '',
        jan2019 : '',
        feb2019 : '$320',
        mar2019 : '$150',
        apr2019 : '',
        may2019 : '',
        jun2019 : '',
        jul2019 : '',
        aug2019 : '',
        sep2019 : '',
        oct2019 : '',
        nov2019 : '',
        dec2019 : ''
    },
    {
        client : 'This is Company F',
        startday : '$250',
        content : '',
        quantity : '$100',
        jan2019 : '$100',
        feb2019 : '',
        mar2019 : '',
        apr2019 : '',
        may2019 : '',
        jun2019 : '',
        jul2019 : '',
        aug2019 : '',
        sep2019 : '',
        oct2019 : '',
        nov2019 : '',
        dec2019 : ''
    },
  ]
  