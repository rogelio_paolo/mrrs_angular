import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CsmClientsComponent } from './csm-clients.component';

describe('CsmClientsComponent', () => {
  let component: CsmClientsComponent;
  let fixture: ComponentFixture<CsmClientsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CsmClientsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CsmClientsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
