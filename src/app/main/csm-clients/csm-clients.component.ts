import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, takeUntil } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';
import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';

import { ClientService } from 'app/main/client_/client.service';
import { MatTableDataSource } from '@angular/material';

@Component({
    selector     : 'csmclient-list',
    templateUrl  : './csm-clients.component.html',
    styleUrls    : ['./csm-clients.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class CsmClientListComponent implements OnInit, OnDestroy
{
    dialogRef: any;
    hasSelectedClient: boolean;
    searchInput: FormControl;
    
    // Private
    private _unsubscribeAll: Subject<any>;
    /**
     * Constructor
     *
     * @param {ClientService} _clientService
     * @param {FuseSidebarService} _fuseSidebarService
     * @param {MatDialog} _matDialog
     */
    constructor(
        private _clientService: ClientService,
        private _fuseSidebarService: FuseSidebarService,
        private _matDialog: MatDialog
    )
    {
        // Set the defaults
        this.searchInput = new FormControl('');

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    displayedColumns: string[] = [
        'client',
        'week1',
        'week2',
        'week3',
        'week4',
        'week5',
        'week6',
        'week7',
        'week8',
        'week9',
        'week10',
        'week11',
        'week12',
        'week13',
        'week14',
        'week15',
        'week16',
        'week17',
        'week18',
        // 'week19',
        // 'week20',
        // 'week21',
        // 'week22',
        // 'week23',
        // 'week24',
        // 'week25',
        // 'week26',
        // 'week27',
        // 'week28',
        // 'week29',
        // 'week30',
        // 'week31',
        // 'week32',
        // 'week33',
        // 'week34',
        // 'week35',
        // 'week36',
        // 'week37',
        // 'week38',
        // 'week39',
        // 'week40',
        // 'week41',
        // 'week42',
        // 'week43',
        // 'week44',
        // 'week45',
        // 'week46',
        // 'week47',
        // 'week48',
        // 'week49',
        // 'week50',
        // 'week51',
        // 'week52',
    ];
    
    dataSource = new MatTableDataSource<TableData>(TABLE_DATA);
    isSticky (column: string): boolean {
        return column === 'client' || column === 'startday' || column === 'content' || column === 'quantity' ? true : false;
    }
    
    ngOnInit(): void
    {
        this._clientService.onSelectedClientsChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(selectClient => {
                this.hasSelectedClient = selectClient.length > 0;
            });

        this.searchInput.valueChanges
            .pipe(
                takeUntil(this._unsubscribeAll),
                debounceTime(300),
                distinctUntilChanged()
            )
            .subscribe(searchText => {
                this._clientService.onSearchTextChanged.next(searchText);
            });
    }

    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    toggleSidebar(name): void
    {
        this._fuseSidebarService.getSidebar(name).toggleOpen();
    }
}

export interface TableData {
    client : string;
    week1:string;
    week2:string;
    week3:string;
    week4:string;
    week5:string;
    week6:string;
    week7:string;
    week8:string;
    week9:string;
    week10:string;
    week11:string;
    week12:string;
    week13:string;
    week14:string;
    week15:string;
    week16:string;
    week17:string;
    week18:string;
    week19:string;
    week20:string;
    week21:string;
    week22:string;
    week23:string;
    week24:string;
    week25:string;
    week26:string;
    week27:string;
    week28:string;
    week29:string;
    week30:string;
    week31:string;
    week32:string;
    week33:string;
    week34:string;
    week35:string;
    week36:string;
    week37:string;
    week38:string;
    week39:string;
    week40:string;
    week41:string;
    week42:string;
    week43:string;
    week44:string;
    week45:string;
    week46:string;
    week47:string;
    week48:string;
    week49:string;
    week50:string;
    week51:string;
    week52:string;
  }
  
  const TABLE_DATA: TableData[] = [
    {
        client : 'This is Company A',
        week1: '',
        week2: '',
        week3: '',
        week4: '',
        week5: '',
        week6: '',
        week7: '',
        week8: '',
        week9: '',
        week10: '',
        week11: '',
        week12: '',
        week13: '',
        week14: '',
        week15: '',
        week16: '',
        week17: '',
        week18: '',
        week19: '',
        week20: '',
        week21: '',
        week22: '',
        week23: '',
        week24: '',
        week25: '',
        week26: '',
        week27: '',
        week28: '',
        week29: '',
        week30: '',
        week31: '',
        week32: '',
        week33: '',
        week34: '',
        week35: '',
        week36: '',
        week37: '',
        week38: '',
        week39: '',
        week40: '',
        week41: '',
        week42: '',
        week43: '',
        week44: '',
        week45: '',
        week46: '',
        week47: '',
        week48: '',
        week49: '',
        week50: '',
        week51: '',
        week52: '',
    },
    {
      client : 'This is Company B',
      week1: '',
      week2: '',
      week3: '',
      week4: '',
      week5: '',
      week6: '',
      week7: '',
      week8: '',
      week9: '',
      week10: '',
      week11: '',
      week12: '',
      week13: '',
      week14: '',
      week15: '',
      week16: '',
      week17: '',
      week18: '',
      week19: '',
      week20: '',
      week21: '',
      week22: '',
      week23: '',
      week24: '',
      week25: '',
      week26: '',
      week27: '',
      week28: '',
      week29: '',
      week30: '',
      week31: '',
      week32: '',
      week33: '',
      week34: '',
      week35: '',
      week36: '',
      week37: '',
      week38: '',
      week39: '',
      week40: '',
      week41: '',
      week42: '',
      week43: '',
      week44: '',
      week45: '',
      week46: '',
      week47: '',
      week48: '',
      week49: '',
      week50: '',
      week51: '',
      week52: '',
  },
  {
    client : 'This is Company C',
    week1: '',
    week2: '',
    week3: '',
    week4: '',
    week5: '',
    week6: '',
    week7: '',
    week8: '',
    week9: '',
    week10: '',
    week11: '',
    week12: '',
    week13: '',
    week14: '',
    week15: '',
    week16: '',
    week17: '',
    week18: '',
    week19: '',
    week20: '',
    week21: '',
    week22: '',
    week23: '',
    week24: '',
    week25: '',
    week26: '',
    week27: '',
    week28: '',
    week29: '',
    week30: '',
    week31: '',
    week32: '',
    week33: '',
    week34: '',
    week35: '',
    week36: '',
    week37: '',
    week38: '',
    week39: '',
    week40: '',
    week41: '',
    week42: '',
    week43: '',
    week44: '',
    week45: '',
    week46: '',
    week47: '',
    week48: '',
    week49: '',
    week50: '',
    week51: '',
    week52: '',
},
{
  client : 'This is Company D',
  week1: '',
  week2: '',
  week3: '',
  week4: '',
  week5: '',
  week6: '',
  week7: '',
  week8: '',
  week9: '',
  week10: '',
  week11: '',
  week12: '',
  week13: '',
  week14: '',
  week15: '',
  week16: '',
  week17: '',
  week18: '',
  week19: '',
  week20: '',
  week21: '',
  week22: '',
  week23: '',
  week24: '',
  week25: '',
  week26: '',
  week27: '',
  week28: '',
  week29: '',
  week30: '',
  week31: '',
  week32: '',
  week33: '',
  week34: '',
  week35: '',
  week36: '',
  week37: '',
  week38: '',
  week39: '',
  week40: '',
  week41: '',
  week42: '',
  week43: '',
  week44: '',
  week45: '',
  week46: '',
  week47: '',
  week48: '',
  week49: '',
  week50: '',
  week51: '',
  week52: '',
},
{
  client : 'This is Company E',
  week1: '',
  week2: '',
  week3: '',
  week4: '',
  week5: '',
  week6: '',
  week7: '',
  week8: '',
  week9: '',
  week10: '',
  week11: '',
  week12: '',
  week13: '',
  week14: '',
  week15: '',
  week16: '',
  week17: '',
  week18: '',
  week19: '',
  week20: '',
  week21: '',
  week22: '',
  week23: '',
  week24: '',
  week25: '',
  week26: '',
  week27: '',
  week28: '',
  week29: '',
  week30: '',
  week31: '',
  week32: '',
  week33: '',
  week34: '',
  week35: '',
  week36: '',
  week37: '',
  week38: '',
  week39: '',
  week40: '',
  week41: '',
  week42: '',
  week43: '',
  week44: '',
  week45: '',
  week46: '',
  week47: '',
  week48: '',
  week49: '',
  week50: '',
  week51: '',
  week52: '',
},
{
  client : 'This is Company F',
  week1: '',
  week2: '',
  week3: '',
  week4: '',
  week5: '',
  week6: '',
  week7: '',
  week8: '',
  week9: '',
  week10: '',
  week11: '',
  week12: '',
  week13: '',
  week14: '',
  week15: '',
  week16: '',
  week17: '',
  week18: '',
  week19: '',
  week20: '',
  week21: '',
  week22: '',
  week23: '',
  week24: '',
  week25: '',
  week26: '',
  week27: '',
  week28: '',
  week29: '',
  week30: '',
  week31: '',
  week32: '',
  week33: '',
  week34: '',
  week35: '',
  week36: '',
  week37: '',
  week38: '',
  week39: '',
  week40: '',
  week41: '',
  week42: '',
  week43: '',
  week44: '',
  week45: '',
  week46: '',
  week47: '',
  week48: '',
  week49: '',
  week50: '',
  week51: '',
  week52: '',
},
{
  client : 'This is Company G',
  week1: '',
  week2: '',
  week3: '',
  week4: '',
  week5: '',
  week6: '',
  week7: '',
  week8: '',
  week9: '',
  week10: '',
  week11: '',
  week12: '',
  week13: '',
  week14: '',
  week15: '',
  week16: '',
  week17: '',
  week18: '',
  week19: '',
  week20: '',
  week21: '',
  week22: '',
  week23: '',
  week24: '',
  week25: '',
  week26: '',
  week27: '',
  week28: '',
  week29: '',
  week30: '',
  week31: '',
  week32: '',
  week33: '',
  week34: '',
  week35: '',
  week36: '',
  week37: '',
  week38: '',
  week39: '',
  week40: '',
  week41: '',
  week42: '',
  week43: '',
  week44: '',
  week45: '',
  week46: '',
  week47: '',
  week48: '',
  week49: '',
  week50: '',
  week51: '',
  week52: '',
},
  ]
  