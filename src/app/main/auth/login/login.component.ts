import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'environments/environment';

@Component({
    selector     : 'login',
    templateUrl  : './login.component.html',
    styleUrls    : ['./login.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class LoginComponent implements OnInit
{
    login:any
    response:any
    resp:any
    userdata:any
    loginForm: FormGroup

    private httpOptions: any

    public errors: any = []
    

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private http: HttpClient,
        private router: Router

    )
    {
        if(localStorage.getItem('authorization') !== null) {
            this.router.navigate(['/dashboard'])
        }

        this._fuseConfigService.config = {
            layout: {
                navbar   : {
                    hidden: true
                },
                toolbar  : {
                    hidden: true
                },
                footer   : {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };

        this.httpOptions = {
            headers: new HttpHeaders({'Content-Type': 'application/json'})
          };
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        this.login = {
            user: '',
            pass: ''
          }

        this.loginForm = this._formBuilder.group({
            user   : ['', [Validators.required]],
            password: ['', Validators.required]
        });
    }

    onSubmit() {
    const creds = {username: this.loginForm.value.user, password: this.loginForm.value.password }
    this.http.post(`${environment.apiUrl}app/auth`, JSON.stringify(creds), this.httpOptions).subscribe(
      response => {
        this.response = response
        
        this.http.get<any>(`${environment.apiUrl}app/heroes/` + this.response.user_drf, this.httpOptions).subscribe(
            resp => {
                this.resp = resp
                const user = {
                    email: this.response.user_email,
                    name: this.response.user_name,
                    role: this.resp.role,
                    dept: this.resp.department,
                    desi: this.resp.designation,
                    drf: this.response.user_drf
                }

                this.userdata = Object.keys(user).map(k => user[k])
                localStorage.setItem('authorization',this.response.token)
                localStorage.setItem('userdata', JSON.stringify(user))
                //this.router.navigate(['dashboard'])
                window.location.href = '/dashboard'
            })
        })
    }
}
