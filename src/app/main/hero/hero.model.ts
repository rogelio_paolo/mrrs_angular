import { FuseUtils } from '@fuse/utils';
export class Hero

{
    user: any;
    user_id: string;
    username: string;
    email: string;
    first_name: string;
    last_name: string;
    role: any;
    role_id: string;
    department:any;
    department_id: string;
    designation:any;
    designation_id: string;
    is_active: string;

    /**
     * 
     * Constructor
     *
     * @param contact
     */
    constructor(hero)
    {
        {
            this.user = {
                "id": hero.user ? hero.user.id : '',
                "username": hero.user ? hero.user.username : '',
                "email": hero.user ? hero.user.email : '',
                "first_name": hero.user ? hero.user.first_name : '',
                "last_name": hero.user ? hero.user.last_name : '',
                "is_active": hero.user ? hero.user.is_active : ''
            },
            this.role = {
                "id": hero.role ? hero.role.id : '',
                "role": hero.role ? hero.role.role : ''
            },
            this.department = {
                "id": hero.department ? hero.department.id : '',
                "department": hero.department ? hero.department.department : ''
            },
            this.designation = {
                "id": hero.designation ? hero.designation.id : '',
                "designation": hero.designation ? hero.designation.designation : ''
            }
            //this.user = hero.user,
            // this.role = hero.role,
            // this.department = hero.department,
            // this.designation = hero.designation
        }
    }
}
