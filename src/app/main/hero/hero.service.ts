import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

import { FuseUtils } from '@fuse/utils';

import { Hero } from 'app/main/hero/hero.model';

import { environment } from 'environments/environment';
@Injectable()
export class HeroService implements Resolve<any>
{
    onHeroesChanged: BehaviorSubject<any>;
    onSelectedHeroesChanged: BehaviorSubject<any>;
    onUserDataChanged: BehaviorSubject<any>;
    onSearchTextChanged: Subject<any>;
    onFilterChanged: Subject<any>;

    heroes: Hero[];
    user: any;
    selectedHeroes: string[] = [];

    searchText: string;
    filterBy: string;

    private httpOptions;
    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    )
    {
        const token = localStorage.getItem('authorization')
        this.httpOptions = {
            headers: new HttpHeaders({'Content-Type': 'application/json', 'Authorization': 'token '+token})
        }

        // Set the defaults
        this.onHeroesChanged = new BehaviorSubject([]);
        this.onSelectedHeroesChanged = new BehaviorSubject([]);
        this.onUserDataChanged = new BehaviorSubject([]);
        this.onSearchTextChanged = new Subject();
        this.onFilterChanged = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getHeroes(),
                this.getUserData()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getHeroes();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getHeroes();
                    });

                    resolve();

                },
                reject
            );
        });
    }

    /**
     * Get contacts
     *
     * @returns {Promise<any>}
     */
    getHeroes(): Promise<any>
    {
        return new Promise((resolve, reject) => {
                this._httpClient.get(`${environment.apiUrl}app/list_users/`,this.httpOptions)
                    .subscribe((response: any) => {

                        this.heroes = response;
                        console.log(this.heroes)

                        if ( this.filterBy === 'starred' )
                        {
                            this.heroes = this.heroes.filter(_hero => {
                                return this.user.starred.includes(_hero.user_id);
                            });
                        }

                        if ( this.filterBy === 'frequent' )
                        {
                            this.heroes = this.heroes.filter(_hero => {
                                return this.user.frequentContacts.includes(_hero.user_id);
                            });
                        }

                        if ( this.searchText && this.searchText !== '' )
                        {
                            this.heroes = FuseUtils.filterArrayByString(this.heroes, this.searchText);
                        }

                        this.heroes = this.heroes.map(hero => {
                            return new Hero(hero);
                        });

                        this.onHeroesChanged.next(this.heroes);
                        resolve(this.heroes);
                    }, reject);
            }
        );
    }

    /**
     * Get user data
     *
     * @returns {Promise<any>}
     */
    getUserData(): Promise<any>
    {
        return new Promise((resolve, reject) => {
                this._httpClient.get('api/contacts-user/5725a6802d10e277a0f35724')
                    .subscribe((response: any) => {
                        this.user = response;
                        this.onUserDataChanged.next(this.user);
                        resolve(this.user);
                    }, reject);
            }
        );
    }

    /**
     * Toggle selected contact by id
     *
     * @param id
     */
    toggleSelectedHero(id): void
    {
        // First, check if we already have that contact as selected...
        if ( this.selectedHeroes.length > 0 )
        {
            const index = this.selectedHeroes.indexOf(id);

            if ( index !== -1 )
            {
                this.selectedHeroes.splice(index, 1);

                // Trigger the next event
                this.onSelectedHeroesChanged.next(this.selectedHeroes);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedHeroes.push(id);

        // Trigger the next event
        this.onSelectedHeroesChanged.next(this.selectedHeroes);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll(): void
    {
        if ( this.selectedHeroes.length > 0 )
        {
            this.deselectHeroes();
        }
        else
        {
            this.selectHeroes();
        }
    }

    /**
     * Select contacts
     *
     * @param filterParameter
     * @param filterValue
     */
    selectHeroes(filterParameter?, filterValue?): void
    {
        this.selectedHeroes = [];

        // If there is no filter, select all contacts
        if ( filterParameter === undefined || filterValue === undefined )
        {
            this.selectedHeroes = [];
            this.heroes.map(hero => {
                this.selectedHeroes.push(hero.user_id);
            });
        }

        // Trigger the next event
        this.onSelectedHeroesChanged.next(this.selectedHeroes);
    }

    /**
     * Update contact
     *
     * @param contact
     * @returns {Promise<any>}
     */
    createHero(hero): Promise<any>
    {   
        return new Promise((resolve, reject) => {
        this._httpClient.post(`${environment.apiUrl}app/heroes/`, {...hero},this.httpOptions)
            .subscribe(response => {
                this.getHeroes()
                resolve(response)
            })
        })
    }

    updateHero(hero): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._httpClient.patch(`${environment.apiUrl}app/heroes/` + hero.user_id + '/', {...hero},this.httpOptions)
                .subscribe(response => {
                    this.getHeroes()
                    resolve(response)
                })
        })
    }

    /**
     * Update user data
     *
     * @param userData
     * @returns {Promise<any>}
     */
    updateUserData(userData): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._httpClient.post('api/contacts-user/' + this.user.id, {...userData})
                .subscribe(response => {
                    this.getUserData();
                    this.getHeroes();
                    resolve(response);
                });
        });
    }

    /**
     * Deselect contacts
     */
    deselectHeroes(): void
    {
        this.selectedHeroes = [];

        // Trigger the next event
        this.onSelectedHeroesChanged.next(this.selectedHeroes);
    }

    /**
     * Delete hero
     *
     * @param hero
     */
    deleteHero(hero): Promise<any>
    {
        // const heroIndex = this.heroes.indexOf(hero);
        // this.heroes.splice(heroIndex, 1);
        // this.onHeroesChanged.next(this.heroes);

        return new Promise((resolve, reject) => {
            this._httpClient.delete('http://localhost:8000/app/users/' + hero.user.id + '/',this.httpOptions)
                .subscribe(response => {
                    this.getHeroes()
                    resolve(response)
                })
        })
    }

    /**
     * Delete selected contacts
     */
    deleteSelectedHeroes(): void
    {
        for ( const heroId of this.selectedHeroes )
        {
            const hero = this.heroes.find(_hero => {
                return _hero.user_id === heroId;
            });
            const heroIndex = this.heroes.indexOf(hero);
            this.heroes.splice(heroIndex, 1);
        }
        this.onHeroesChanged.next(this.heroes);
        this.deselectHeroes();
    }

}
