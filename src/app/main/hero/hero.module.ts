import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatRippleModule } from '@angular/material/core';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { AgmCoreModule } from '@agm/core';
import { MatMenuModule} from '@angular/material/menu';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatToolbarModule } from '@angular/material';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatCheckboxModule } from '@angular/material/checkbox';

import { FuseSharedModule } from '@fuse/shared.module';
import { FuseWidgetModule } from '@fuse/components/widget/widget.module';

import { HeroListComponent } from 'app/main/hero/list/list.component';
import { HeroListService } from 'app/main/hero/list/list.service';
// import { heroInnerComponent } from './inner/inner.component';
// import { heroInnerService } from './inner/inner.service';
import { HeroComponent } from 'app/main/hero/hero.component';
import { HeroService } from 'app/main/hero/hero.service';
import { HeroSelectedBarComponent } from 'app/main/hero/selected-bar/selected-bar.component';
import { HeroMainSidebarComponent } from 'app/main/hero/sidebars/main/main.component';
import { HeroFormDialogComponent } from 'app/main/hero/form/form.component';
const routes: Routes = [
    {
        path     : 'hero/list',
        component: HeroComponent,
        resolve  : {
            data: HeroService
        }
    },
    // {
    //     path     : 'client/profile/:id',
    //     component: ClientInnerComponent,
    //     resolve  : {
    //         data: ClientInnerService
    //     }
    // }
];

@NgModule({
    declarations: [
        HeroComponent,
        HeroListComponent,
        HeroSelectedBarComponent,
        HeroMainSidebarComponent,
        HeroFormDialogComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatChipsModule,
        MatExpansionModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatPaginatorModule,
        MatRippleModule,
        MatSelectModule,
        MatSortModule,
        MatSnackBarModule,
        MatTableModule,
        MatTabsModule,
        MatMenuModule,
        MatToolbarModule,
        FormsModule,
        ReactiveFormsModule,
        MatDialogModule,
        MatCheckboxModule,
        
        NgxChartsModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
        }),

        FuseSharedModule,
        FuseWidgetModule
    ],
    providers   : [
        HeroService
    ],
    entryComponents: [HeroFormDialogComponent]
})
export class HeroModule
{
}
