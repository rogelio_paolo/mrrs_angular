import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

import { Hero } from 'app/main/hero/hero.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from 'environments/environment';
export interface Roles {
  value: string;
  viewValue: string;
}

@Component({
    selector     : 'hero-form-dialog',
    templateUrl  : './form.component.html',
    styleUrls    : ['./form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class HeroFormDialogComponent
{
    action: string
    hero: Hero
    heroForm: FormGroup
    roles: any
    departments: any
    designations: any
    selectedDept: string
    dialogTitle: string

    httpHeaders: any

    /**
     * Constructor
     *
     * @param {MatDialogRef<HeroFormDialogComponent>} matDialogRef
     * @param _data
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        public matDialogRef: MatDialogRef<HeroFormDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private _formBuilder: FormBuilder,
        private _httpClient: HttpClient
    )
    {
        const token = localStorage.getItem('authorization')
        this.httpHeaders = {
            headers: new HttpHeaders({'Content-Type': 'application/json', 'Authorization': 'token '+token})
        }

        this.action = _data.action;

        if ( this.action === 'edit' )
        {
            this.dialogTitle = 'Edit Hero';
            this.hero = _data.hero;
            this.heroForm = this.updateHeroForm();
        }
        else    
        {
            this.dialogTitle = 'New Hero';
            this.hero = new Hero({});
            this.heroForm = this.createHeroForm();
        }

        this.getRoles()
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Create contact form
     *
     * @returns {FormGroup}
     */
    

    getRoles()
    {
        this._httpClient.get(`${environment.apiUrl}app/list_roles/`, this.httpHeaders).subscribe((list_roles: any) => {
            const list_roles_arr = []
            for (let key in list_roles) {
                list_roles_arr[key] = {
                    label: list_roles[key].role,
                    id: list_roles[key].id
                }
                // Use `key` and `value`
            }
            this.roles = list_roles_arr
        })

        this._httpClient.get(`${environment.apiUrl}app/list_departments/`, this.httpHeaders).subscribe((list_departments: any) => {
            const list_departments_arr = []
            for (let key in list_departments) {
                list_departments_arr[key] = {
                    label: list_departments[key].department,
                    id: list_departments[key].id
                }
                // Use `key` and `value`
            }
            this.departments = list_departments_arr
        })

        this._httpClient.get(`${environment.apiUrl}app/list_designations/?department=` + this.heroForm.controls.department.value, this.httpHeaders).subscribe((response: any) => {
            const list_designations = response.designations
            const list_designations_arr = []
            for (let key in list_designations) {
                list_designations_arr[key] = {
                    label: list_designations[key].designation,
                    id: list_designations[key].id
                }
                // Use `key` and `value`
            }
            this.designations = list_designations_arr
        })

        // this.roles.forEach(obj => {
        //     obj.forEach(roles=> {
        //         value = obj.role_code;
        //         viewValue = obj.role;
        //     })
        // })

        // console.log(roles)

        // roles: Roles[] = [
        //     {value: 'steak-0', viewValue: 'Steak'},
        //     {value: 'pizza-1', viewValue: 'Pizza'},
        //     {value: 'tacos-2', viewValue: 'Tacos'}
        //   ];

    }


    onChangedDept(dept)
    {
        this._httpClient.get(`${environment.apiUrl}app/list_designations/?department=` + dept.value, this.httpHeaders).subscribe((response: any) => {
            const list_designations = response.designations
            const list_designations_arr = []
            for (let key in list_designations) {
                list_designations_arr[key] = {
                    label: list_designations[key].designation,
                    id: list_designations[key].id
                }
                // Use `key` and `value`
            }
            this.designations = list_designations_arr
        })
    }

    createHeroForm(): FormGroup
    {
        return this._formBuilder.group({
            user : this._formBuilder.group({
                username     : [this.hero.user.username],
                first_name   : [this.hero.user.first_name],
                last_name    : [this.hero.user.last_name],
                email        : [this.hero.user.email],
            }),
            // role : this._formBuilder.group({
            //     role: [this.hero.role.role]
            // }),
            // department: this._formBuilder.group({
            //     department: [this.hero.department.department]
            // }),
            // designation: this._formBuilder.group({
            //     designation: [this.hero.designation.designation]
            // })
            role         : [this.hero.role.id],
            department   : [this.hero.department.id],
            designation  : [this.hero.designation.id],
            user_id      : [this.hero.user.id],
        })
    }


    updateHeroForm(): FormGroup
    {
        return this._formBuilder.group({
            user : this._formBuilder.group({
                first_name   : [this.hero.user.first_name],
                last_name    : [this.hero.user.last_name],
                email        : [this.hero.user.email]
            }),
            // role : this._formBuilder.group({
            //     id: [this.hero.role.id],
            //     role: [this.hero.role.role]
            // }),
            // department: this._formBuilder.group({
            //     id: [this.hero.department.id],
            //     department: [this.hero.department.department]
            // }),
            // designation: this._formBuilder.group({
            //     id: [this.hero.designation.id],
            //     designation: [this.hero.designation.designation]
            // })
            role         : [this.hero.role.id],
            department   : [this.hero.department.id],
            designation  : [this.hero.designation.id],
            user_id      : [this.hero.user.id],
        })
    }

}
