import { Component, OnDestroy, OnInit, TemplateRef, ViewChild, ViewEncapsulation, ElementRef } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject, fromEvent, merge, Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { takeUntil } from 'rxjs/internal/operators';

import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';

import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';

import { HeroService } from 'app/main/hero/hero.service';
import { HeroFormDialogComponent } from 'app/main/hero/form/form.component';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
    selector     : 'hero-list',
    templateUrl  : './list.component.html',
    styleUrls    : ['./list.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class HeroListComponent implements OnInit, OnDestroy
{
    @ViewChild('dialogContent', {static: false})
    dialogContent: TemplateRef<any>

    heroes: any
    user: any

    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'avatar', 'first_name', 'email', 'role', 'department', 'designation']

    @ViewChild(MatPaginator, {static: true})
    paginator: MatPaginator;

    @ViewChild('filter', {static: true})
    filter: ElementRef;

    @ViewChild(MatSort, {static: true})
    sort: MatSort;

    selectedHeroes: any[]
    checkboxes: {}
    dialogRef: any
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>

    httpOptions: any

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {HeroService} _heroService
     * @param {MatDialog} _matDialog
     */
    constructor(
        private _heroService: HeroService,
        public _matDialog: MatDialog,
        private _httpClient: HttpClient,
        private _router: Router
    )
    {
        const userdata = JSON.parse(localStorage.getItem('userdata'))
        if(userdata !== null) {
            if(userdata.role !== 1 && userdata.role !== 4) {
                this._router.navigate(['dashboard'])
            }
        }
        else {
            this._router.navigate(['/auth/login'])
        }

        this._unsubscribeAll = new Subject();

        const token = localStorage.getItem('authorization')
        this.httpOptions = {
            headers: new HttpHeaders({'Content-Type': 'application/json', 'Authorization': 'token '+token})
        }
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        this.dataSource = new FilesDataSource(this._heroService, this.paginator, this.sort);

        this._heroService.onHeroesChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(heroes => {
                this.heroes = heroes;
                console.log(heroes)
                this.checkboxes = {};
                heroes.map(hero => {
                    this.checkboxes[hero.user_id] = false;
                });
            });

        this._heroService.onSelectedHeroesChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(selectedHeroes => {
                for ( const user_id in this.checkboxes )
                {
                    if ( !this.checkboxes.hasOwnProperty(user_id) )
                    {
                        continue;
                    }

                    this.checkboxes[user_id] = selectedHeroes.includes(user_id);
                }
                this.selectedHeroes = selectedHeroes;
            });

        this._heroService.onUserDataChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(user => {
                this.user = user;
            });

        this._heroService.onFilterChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                this._heroService.deselectHeroes();
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Edit hero
     *
     * @param hero
     */
    editHero(hero): void
    {
        this.dialogRef = this._matDialog.open(HeroFormDialogComponent, {
            panelClass: 'hero-form-dialog',
            data      : {
                hero: hero,
                action : 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if ( !response )
                {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch ( actionType )
                {
                    /**
                     * Save
                     */
                    case 'save':

                        this._heroService.updateHero(formData.getRawValue());

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteHero(hero);

                        break;
                }
            });
    }

    /**
     * Delete Contact
     */
    deleteHero(hero): void
    {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this._heroService.deleteHero(hero);
            }
            this.confirmDialogRef = null;
        });

    }

    /**
     * On selected change
     *
     * @param heroId
     */
    onSelectedChange(heroId): void
    {
        this._heroService.toggleSelectedHero(heroId);
    }

    /**
     * Toggle star
     *
     * @param heroId
     */
    toggleStar(heroId): void
    {
        if ( this.user.starred.includes(heroId) )
        {
            this.user.starred.splice(this.user.starred.indexOf(heroId), 1);
        }
        else
        {
            this.user.starred.push(heroId);
        }

        this._heroService.updateUserData(this.user);
    }
}

export class FilesDataSource extends DataSource<any>
{
    private _filterChange = new BehaviorSubject('');
    private _filteredDataChange = new BehaviorSubject('');

    constructor(
        private _heroService: HeroService,
        private _matPaginator: MatPaginator,
        private _matSort: MatSort
    )
    {
        super()

        this.filteredData = this._heroService.heroes
    }

    get filteredData(): any
    {
        return this._filteredDataChange.value;
    }

    set filteredData(value: any)
    {
        this._filteredDataChange.next(value);
    }

    // Filter
    get filter(): string
    {
        return this._filterChange.value;
    }

    set filter(filter: string)
    {
        this._filterChange.next(filter);
    }

    connect(): Observable<any[]>
    {
        const displayDataChanges = [
            this._heroService.onHeroesChanged,
            this._matPaginator.page,
            this._filterChange,
            this._matSort.sortChange
        ];

        return merge(...displayDataChanges).pipe(map(() => {

                let data = this._heroService.heroes.slice();

                data = this.filterData(data);

                this.filteredData = [...data];

                data = this.sortData(data);

                // Grab the page's slice of data.
                const startIndex = this._matPaginator.pageIndex * this._matPaginator.pageSize;
                return data.splice(startIndex, this._matPaginator.pageSize);
            })
        );

    }

    /**
     * Filter data
     *
     * @param data
     * @returns {any}
     */
    filterData(data): any
    {
        if ( !this.filter )
        {
            return data;
        }
        return FuseUtils.filterArrayByString(data, this.filter);
    }

    /**
     * Sort data
     *
     * @param data
     * @returns {any[]}
     */
    sortData(data): any[]
    {
        if ( !this._matSort.active || this._matSort.direction === '' )
        {
            return data;
        }

        return data.sort((a, b) => {
            let propertyA: number | string = '';
            let propertyB: number | string = '';

            switch ( this._matSort.active )
            {
                case 'first_name':
                    [propertyA, propertyB] = [a.user.first_name, b.user.first_name];
                    break;
                case 'email':
                    [propertyA, propertyB] = [a.user.email, b.user.email];
                    break;
                case 'role':
                    [propertyA, propertyB] = [a.role.role, b.role.role];
                    break;
                case 'department':
                    [propertyA, propertyB] = [a.department.department, b.department.department];
                    break;
                case 'designation':
                    [propertyA, propertyB] = [a.designation.designation, b.designation.designation];
                    break;
            }

            const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
            const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

            return (valueA < valueB ? -1 : 1) * (this._matSort.direction === 'asc' ? 1 : -1);
        });
    }

    disconnect(): void
    {
    }
}
