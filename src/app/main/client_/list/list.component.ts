import { Component, OnDestroy, OnInit, TemplateRef, ViewChild, ViewEncapsulation, ElementRef } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject, fromEvent, merge, Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { takeUntil } from 'rxjs/internal/operators';

import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';

import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';

import { ClientService } from 'app/main/client_/client.service';
import { ClientFormDialogComponent } from 'app/main/client_/form/form.component';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material';

@Component({
    selector     : 'client-list',
    templateUrl  : './list.component.html',
    styleUrls    : ['./list.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ClientListComponent implements OnInit, OnDestroy
{
    @ViewChild('dialogContent', {static: false})
    dialogContent: TemplateRef<any>

    clients: any
    user: any

    columnSource = new MatTableDataSource<any>(null);
    dataSource: FilesDataSource | null;
    displayedColumns = ['id', 'client', 'services', 'contents', 'kpi', 'start_date', 'created_at']

    @ViewChild(MatPaginator, {static: true})
    paginator: MatPaginator;

    @ViewChild('filter', {static: true})
    filter: ElementRef;

    @ViewChild(MatSort, {static: true})
    sort: MatSort;

    selectedClients: any[]
    checkboxes: {}
    dialogRef: any
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>

    httpOptions: any

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {ClientService} _clientService
     * @param {MatDialog} _matDialog
     */
    constructor(
        private _clientService: ClientService,
        public _matDialog: MatDialog,
        private _httpClient: HttpClient,
        private _router: Router
    )
    {
        const userdata = JSON.parse(localStorage.getItem('userdata'))
        if(userdata !== null) {
            if(userdata.role !== 1 || (userdata.role !== 1 && userdata.department !== 4)) {
                this._router.navigate(['dashboard'])
            }
        }
        else {
            this._router.navigate(['/auth/login'])
        }

        this._unsubscribeAll = new Subject();

        const token = localStorage.getItem('authorization')
        this.httpOptions = {
            headers: new HttpHeaders({'Content-Type': 'application/json', 'Authorization': 'token '+token})
        }
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        this.dataSource = new FilesDataSource(this._clientService, this.paginator, this.sort);

        this._clientService.onClientsChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(clients => {
                this.clients = clients;
                console.log(clients)
                this.checkboxes = {};
                clients.map(client => {
                    this.checkboxes[client.id] = false;
                });
            });

        this._clientService.onSelectedClientsChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(selectedClients => {
                for ( const id in this.checkboxes )
                {
                    if ( !this.checkboxes.hasOwnProperty(id) )
                    {
                        continue;
                    }

                    this.checkboxes[id] = selectedClients.includes(id);
                }
                this.selectedClients = selectedClients;
            });

        this._clientService.onUserDataChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(user => {
                this.user = user;
            });

        this._clientService.onFilterChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                this._clientService.deselectClients();
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Edit client
     *
     * @param client
     */
    editClient(client): void
    {
        this.dialogRef = this._matDialog.open(ClientFormDialogComponent, {
            panelClass: 'client-form-dialog',
            data      : {
                client: client,
                action : 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if ( !response )
                {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch ( actionType )
                {
                    /**
                     * Save
                     */
                    case 'save':

                        this._clientService.updateClient(formData.getRawValue());

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteClient(client);

                        break;
                }
            });
    }

    /**
     * Delete Contact
     */
    deleteClient(client): void
    {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this._clientService.deleteClient(client);
            }
            this.confirmDialogRef = null;
        });

    }

    /**
     * On selected change
     *
     * @param clientId
     */
    onSelectedChange(clientId): void
    {
        this._clientService.toggleSelectedClient(clientId);
    }

    /**
     * Toggle star
     *
     * @param clientId
     */
    // toggleStar(clientId): void
    // {
    //     if ( this.user.starred.includes(clientId) )
    //     {
    //         this.user.starred.splice(this.user.starred.indexOf(clientId), 1);
    //     }
    //     else
    //     {
    //         this.user.starred.push(clientId);
    //     }

    //     this._clientService.updateUserData(this.user);
    // }
}

export class FilesDataSource extends DataSource<any>
{
    private _filterChange = new BehaviorSubject('');
    private _filteredDataChange = new BehaviorSubject('');

    constructor(
        private _clientService: ClientService,
        private _matPaginator: MatPaginator,
        private _matSort: MatSort
    )
    {
        super()

        this.filteredData = this._clientService.clients
    }

    get filteredData(): any
    {
        return this._filteredDataChange.value;
    }

    set filteredData(value: any)
    {
        this._filteredDataChange.next(value);
    }

    // Filter
    get filter(): string
    {
        return this._filterChange.value;
    }

    set filter(filter: string)
    {
        this._filterChange.next(filter);
    }

    connect(): Observable<any[]>
    {
        const displayDataChanges = [
            this._clientService.onClientsChanged,
            this._matPaginator.page,
            this._filterChange,
            this._matSort.sortChange
        ];

        return merge(...displayDataChanges).pipe(map(() => {

                let data = this._clientService.clients.slice();

                data = this.filterData(data);

                this.filteredData = [...data];

                data = this.sortData(data);

                // Grab the page's slice of data.
                const startIndex = this._matPaginator.pageIndex * this._matPaginator.pageSize;
                return data.splice(startIndex, this._matPaginator.pageSize);
            })
        );

    }

    /**
     * Filter data
     *
     * @param data
     * @returns {any}
     */
    filterData(data): any
    {
        if ( !this.filter )
        {
            return data;
        }
        return FuseUtils.filterArrayByString(data, this.filter);
    }

    /**
     * Sort data
     *
     * @param data
     * @returns {any[]}
     */
    sortData(data): any[]
    {
        if ( !this._matSort.active || this._matSort.direction === '' )
        {
            return data;
        }

        return data.sort((a, b) => {
            let propertyA: number | string = '';
            let propertyB: number | string = '';

            switch ( this._matSort.active )
            {
                case 'client_name':
                    [propertyA, propertyB] = [a.client_name, b.client_name];
                    break;
                case 'created_at':
                    [propertyA, propertyB] = [a.created_at, b.created_at];
                    break;
                case 'start_date':
                    [propertyA, propertyB] = [a.start_date, b.start_date];
                    break;
            }

            const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
            const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

            return (valueA < valueB ? -1 : 1) * (this._matSort.direction === 'asc' ? 1 : -1);
        });
    }

    disconnect(): void
    {
    }
    
}
