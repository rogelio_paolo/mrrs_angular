import { FuseUtils } from '@fuse/utils';
export class ClientManager

{
    client: any
    services: any
    contents: any
    kpis: any

    /**
     * 
     * Constructor
     *
     * @param contact
     */
    constructor(client?)
    {
        {
            this.client = client.client || ""
            this.services = client.services || ""
            this.contents = client.contents || ""
            this.kpis = client.kpis || ""
        }
    }
}
