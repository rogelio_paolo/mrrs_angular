import { Component, Inject, ViewEncapsulation, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray, NgForm } from '@angular/forms'
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

import { Client } from 'app/main/client_/client.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Countries } from 'app/main/client_/countries.model';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { MAT_DATE_LOCALE } from '@angular/material';
import { environment } from 'environments/environment';

export interface Roles {
  value: string;
  viewValue: string;
}

@Component({
    selector     : 'client-form-dialog',
    templateUrl  : './form.component.html',
    styleUrls    : ['./form.component.scss'],
    encapsulation: ViewEncapsulation.None,
    providers: [
        {provide: MAT_DATE_LOCALE, useValue: 'en-GB'}
    ]
})

export class ClientFormDialogComponent
{
    _client: FormGroup
    action: string
    clientdata: Client
    clientForm: FormGroup
    service_list: any
    _service: any
    service_type: string
    pms: any
    content_list: any
    writers: any
    kpi: any
    duration: any
    _duration = ""
    industries: any
    enddate = ""
    startdate = ""
    createdby = ""
    dialogTitle: string

    // countries array
    countries: any

    minDate = new Date()
    maxDate = new Date(new Date().setFullYear(new Date().getFullYear() + 20))

    dateFilter = (d: Date): boolean => {
        const date = d.getDate()
        return date !== 29 && date !== 30 && date !== 31
    }

    @Output() 
    dateChange:EventEmitter< MatDatepickerInputEvent< any>>;

    httpHeaders: any

    _clientdata = new FormGroup({
        xero_id:  new FormControl(''),
        client_name:  new FormControl(''),
        created: new FormControl(''),
        pm:  new FormControl(''),
        writer:  new FormControl(''),
        duration:  new FormControl(''),
        start_date:  new FormControl(''),
        end_date:  new FormControl(''),
        datecreated:  new FormControl(''),
        industry:  new FormControl(''),
        other_revenue:  new FormControl(''),
        media_fees:  new FormControl(''),
        contract:  new FormControl(''),
        source:  new FormControl(''),
        company_size:  new FormControl('')
    })

    /**
     * Constructor
     *
     * @param {MatDialogRef<HeroFormDialogComponent>} matDialogRef
     * @param _data
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        public matDialogRef: MatDialogRef<ClientFormDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private _formBuilder: FormBuilder,
        private _httpClient: HttpClient
    )
    {
        const token = localStorage.getItem('authorization')
        this.httpHeaders = {
            headers: new HttpHeaders({'Content-Type': 'application/json', 'Authorization': 'token '+token})
        }

        this.action = _data.action;

        if ( this.action === 'edit' )
        {
            this.dialogTitle = 'Edit Client'
            this.clientdata = _data.client
            this.clientForm = this._client
        }
        else    
        {
            this.dialogTitle = 'New Client'
            this.createdby = JSON.parse(localStorage.getItem('userdata')).drf
            this.clientdata = new Client({})
            this.clientForm = this.createClientForm()
        }

        this.getSelectComponents()

    }

    ngOnInit() {
        //this.addServiceForm.addControl('rows', this.serviceRows);
        //this.addContentForm.addControl('rows', this.contentRows);
        this.countries = [
            {"name": "Afghanistan", "code": "AF"}, 
            {"name": "land Islands", "code": "AX"}, 
            {"name": "Albania", "code": "AL"}, 
            {"name": "Algeria", "code": "DZ"}, 
            {"name": "American Samoa", "code": "AS"}, 
            {"name": "AndorrA", "code": "AD"}, 
            {"name": "Angola", "code": "AO"}, 
            {"name": "Anguilla", "code": "AI"}, 
            {"name": "Antarctica", "code": "AQ"}, 
            {"name": "Antigua and Barbuda", "code": "AG"}, 
            {"name": "Argentina", "code": "AR"}, 
            {"name": "Armenia", "code": "AM"}, 
            {"name": "Aruba", "code": "AW"}, 
            {"name": "Australia", "code": "AU"}, 
            {"name": "Austria", "code": "AT"}, 
            {"name": "Azerbaijan", "code": "AZ"}, 
            {"name": "Bahamas", "code": "BS"}, 
            {"name": "Bahrain", "code": "BH"}, 
            {"name": "Bangladesh", "code": "BD"}, 
            {"name": "Barbados", "code": "BB"}, 
            {"name": "Belarus", "code": "BY"}, 
            {"name": "Belgium", "code": "BE"}, 
            {"name": "Belize", "code": "BZ"}, 
            {"name": "Benin", "code": "BJ"}, 
            {"name": "Bermuda", "code": "BM"}, 
            {"name": "Bhutan", "code": "BT"}, 
            {"name": "Bolivia", "code": "BO"}, 
            {"name": "Bosnia and Herzegovina", "code": "BA"}, 
            {"name": "Botswana", "code": "BW"}, 
            {"name": "Bouvet Island", "code": "BV"}, 
            {"name": "Brazil", "code": "BR"}, 
            {"name": "British Indian Ocean Territory", "code": "IO"}, 
            {"name": "Brunei Darussalam", "code": "BN"}, 
            {"name": "Bulgaria", "code": "BG"}, 
            {"name": "Burkina Faso", "code": "BF"}, 
            {"name": "Burundi", "code": "BI"}, 
            {"name": "Cambodia", "code": "KH"}, 
            {"name": "Cameroon", "code": "CM"}, 
            {"name": "Canada", "code": "CA"}, 
            {"name": "Cape Verde", "code": "CV"}, 
            {"name": "Cayman Islands", "code": "KY"}, 
            {"name": "Central African Republic", "code": "CF"}, 
            {"name": "Chad", "code": "TD"}, 
            {"name": "Chile", "code": "CL"}, 
            {"name": "China", "code": "CN"}, 
            {"name": "Christmas Island", "code": "CX"}, 
            {"name": "Cocos (Keeling) Islands", "code": "CC"}, 
            {"name": "Colombia", "code": "CO"}, 
            {"name": "Comoros", "code": "KM"}, 
            {"name": "Congo", "code": "CG"}, 
            {"name": "Congo, The Democratic Republic of the", "code": "CD"}, 
            {"name": "Cook Islands", "code": "CK"}, 
            {"name": "Costa Rica", "code": "CR"}, 
            {"name": "Cote D'Ivoire", "code": "CI"}, 
            {"name": "Croatia", "code": "HR"}, 
            {"name": "Cuba", "code": "CU"}, 
            {"name": "Cyprus", "code": "CY"}, 
            {"name": "Czech Republic", "code": "CZ"}, 
            {"name": "Denmark", "code": "DK"}, 
            {"name": "Djibouti", "code": "DJ"}, 
            {"name": "Dominica", "code": "DM"}, 
            {"name": "Dominican Republic", "code": "DO"}, 
            {"name": "Ecuador", "code": "EC"}, 
            {"name": "Egypt", "code": "EG"}, 
            {"name": "El Salvador", "code": "SV"}, 
            {"name": "Equatorial Guinea", "code": "GQ"}, 
            {"name": "Eritrea", "code": "ER"}, 
            {"name": "Estonia", "code": "EE"}, 
            {"name": "Ethiopia", "code": "ET"}, 
            {"name": "Falkland Islands (Malvinas)", "code": "FK"}, 
            {"name": "Faroe Islands", "code": "FO"}, 
            {"name": "Fiji", "code": "FJ"}, 
            {"name": "Finland", "code": "FI"}, 
            {"name": "France", "code": "FR"}, 
            {"name": "French Guiana", "code": "GF"}, 
            {"name": "French Polynesia", "code": "PF"}, 
            {"name": "French Southern Territories", "code": "TF"}, 
            {"name": "Gabon", "code": "GA"}, 
            {"name": "Gambia", "code": "GM"}, 
            {"name": "Georgia", "code": "GE"}, 
            {"name": "Germany", "code": "DE"}, 
            {"name": "Ghana", "code": "GH"}, 
            {"name": "Gibraltar", "code": "GI"}, 
            {"name": "Greece", "code": "GR"}, 
            {"name": "Greenland", "code": "GL"}, 
            {"name": "Grenada", "code": "GD"}, 
            {"name": "Guadeloupe", "code": "GP"}, 
            {"name": "Guam", "code": "GU"}, 
            {"name": "Guatemala", "code": "GT"}, 
            {"name": "Guernsey", "code": "GG"}, 
            {"name": "Guinea", "code": "GN"}, 
            {"name": "Guinea-Bissau", "code": "GW"}, 
            {"name": "Guyana", "code": "GY"}, 
            {"name": "Haiti", "code": "HT"}, 
            {"name": "Heard Island and Mcdonald Islands", "code": "HM"}, 
            {"name": "Holy See (Vatican City State)", "code": "VA"}, 
            {"name": "Honduras", "code": "HN"}, 
            {"name": "Hong Kong", "code": "HK"}, 
            {"name": "Hungary", "code": "HU"}, 
            {"name": "Iceland", "code": "IS"}, 
            {"name": "India", "code": "IN"}, 
            {"name": "Indonesia", "code": "ID"}, 
            {"name": "Iran, Islamic Republic Of", "code": "IR"}, 
            {"name": "Iraq", "code": "IQ"}, 
            {"name": "Ireland", "code": "IE"}, 
            {"name": "Isle of Man", "code": "IM"}, 
            {"name": "Israel", "code": "IL"}, 
            {"name": "Italy", "code": "IT"}, 
            {"name": "Jamaica", "code": "JM"}, 
            {"name": "Japan", "code": "JP"}, 
            {"name": "Jersey", "code": "JE"}, 
            {"name": "Jordan", "code": "JO"}, 
            {"name": "Kazakhstan", "code": "KZ"}, 
            {"name": "Kenya", "code": "KE"}, 
            {"name": "Kiribati", "code": "KI"}, 
            {"name": "Korea, Democratic People'S Republic of", "code": "KP"}, 
            {"name": "Korea, Republic of", "code": "KR"}, 
            {"name": "Kuwait", "code": "KW"}, 
            {"name": "Kyrgyzstan", "code": "KG"}, 
            {"name": "Lao People'S Democratic Republic", "code": "LA"}, 
            {"name": "Latvia", "code": "LV"}, 
            {"name": "Lebanon", "code": "LB"}, 
            {"name": "Lesotho", "code": "LS"}, 
            {"name": "Liberia", "code": "LR"}, 
            {"name": "Libyan Arab Jamahiriya", "code": "LY"}, 
            {"name": "Liechtenstein", "code": "LI"}, 
            {"name": "Lithuania", "code": "LT"}, 
            {"name": "Luxembourg", "code": "LU"}, 
            {"name": "Macao", "code": "MO"}, 
            {"name": "Macedonia, The Former Yugoslav Republic of", "code": "MK"}, 
            {"name": "Madagascar", "code": "MG"}, 
            {"name": "Malawi", "code": "MW"}, 
            {"name": "Malaysia", "code": "MY"}, 
            {"name": "Maldives", "code": "MV"}, 
            {"name": "Mali", "code": "ML"}, 
            {"name": "Malta", "code": "MT"}, 
            {"name": "Marshall Islands", "code": "MH"}, 
            {"name": "Martinique", "code": "MQ"}, 
            {"name": "Mauritania", "code": "MR"}, 
            {"name": "Mauritius", "code": "MU"}, 
            {"name": "Mayotte", "code": "YT"}, 
            {"name": "Mexico", "code": "MX"}, 
            {"name": "Micronesia, Federated States of", "code": "FM"}, 
            {"name": "Moldova, Republic of", "code": "MD"}, 
            {"name": "Monaco", "code": "MC"}, 
            {"name": "Mongolia", "code": "MN"}, 
            {"name": "Montenegro", "code": "ME"},
            {"name": "Montserrat", "code": "MS"},
            {"name": "Morocco", "code": "MA"}, 
            {"name": "Mozambique", "code": "MZ"}, 
            {"name": "Myanmar", "code": "MM"}, 
            {"name": "Namibia", "code": "NA"}, 
            {"name": "Nauru", "code": "NR"}, 
            {"name": "Nepal", "code": "NP"}, 
            {"name": "Netherlands", "code": "NL"}, 
            {"name": "Netherlands Antilles", "code": "AN"}, 
            {"name": "New Caledonia", "code": "NC"}, 
            {"name": "New Zealand", "code": "NZ"}, 
            {"name": "Nicaragua", "code": "NI"}, 
            {"name": "Niger", "code": "NE"}, 
            {"name": "Nigeria", "code": "NG"}, 
            {"name": "Niue", "code": "NU"}, 
            {"name": "Norfolk Island", "code": "NF"}, 
            {"name": "Northern Mariana Islands", "code": "MP"}, 
            {"name": "Norway", "code": "NO"}, 
            {"name": "Oman", "code": "OM"}, 
            {"name": "Pakistan", "code": "PK"}, 
            {"name": "Palau", "code": "PW"}, 
            {"name": "Palestinian Territory, Occupied", "code": "PS"}, 
            {"name": "Panama", "code": "PA"}, 
            {"name": "Papua New Guinea", "code": "PG"}, 
            {"name": "Paraguay", "code": "PY"}, 
            {"name": "Peru", "code": "PE"}, 
            {"name": "Philippines", "code": "PH"}, 
            {"name": "Pitcairn", "code": "PN"}, 
            {"name": "Poland", "code": "PL"}, 
            {"name": "Portugal", "code": "PT"}, 
            {"name": "Puerto Rico", "code": "PR"}, 
            {"name": "Qatar", "code": "QA"}, 
            {"name": "Reunion", "code": "RE"}, 
            {"name": "Romania", "code": "RO"}, 
            {"name": "Russian Federation", "code": "RU"}, 
            {"name": "RWANDA", "code": "RW"}, 
            {"name": "Saint Helena", "code": "SH"}, 
            {"name": "Saint Kitts and Nevis", "code": "KN"}, 
            {"name": "Saint Lucia", "code": "LC"}, 
            {"name": "Saint Pierre and Miquelon", "code": "PM"}, 
            {"name": "Saint Vincent and the Grenadines", "code": "VC"}, 
            {"name": "Samoa", "code": "WS"}, 
            {"name": "San Marino", "code": "SM"}, 
            {"name": "Sao Tome and Principe", "code": "ST"}, 
            {"name": "Saudi Arabia", "code": "SA"}, 
            {"name": "Senegal", "code": "SN"}, 
            {"name": "Serbia", "code": "RS"}, 
            {"name": "Seychelles", "code": "SC"}, 
            {"name": "Sierra Leone", "code": "SL"}, 
            {"name": "Singapore", "code": "SG"}, 
            {"name": "Slovakia", "code": "SK"}, 
            {"name": "Slovenia", "code": "SI"}, 
            {"name": "Solomon Islands", "code": "SB"}, 
            {"name": "Somalia", "code": "SO"}, 
            {"name": "South Africa", "code": "ZA"}, 
            {"name": "South Georgia and the South Sandwich Islands", "code": "GS"}, 
            {"name": "Spain", "code": "ES"}, 
            {"name": "Sri Lanka", "code": "LK"}, 
            {"name": "Sudan", "code": "SD"}, 
            {"name": "Suriname", "code": "SR"}, 
            {"name": "Svalbard and Jan Mayen", "code": "SJ"}, 
            {"name": "Swaziland", "code": "SZ"}, 
            {"name": "Sweden", "code": "SE"}, 
            {"name": "Switzerland", "code": "CH"}, 
            {"name": "Syrian Arab Republic", "code": "SY"}, 
            {"name": "Taiwan, Province of China", "code": "TW"}, 
            {"name": "Tajikistan", "code": "TJ"}, 
            {"name": "Tanzania, United Republic of", "code": "TZ"}, 
            {"name": "Thailand", "code": "TH"}, 
            {"name": "Timor-Leste", "code": "TL"}, 
            {"name": "Togo", "code": "TG"}, 
            {"name": "Tokelau", "code": "TK"}, 
            {"name": "Tonga", "code": "TO"}, 
            {"name": "Trinidad and Tobago", "code": "TT"}, 
            {"name": "Tunisia", "code": "TN"}, 
            {"name": "Turkey", "code": "TR"}, 
            {"name": "Turkmenistan", "code": "TM"}, 
            {"name": "Turks and Caicos Islands", "code": "TC"}, 
            {"name": "Tuvalu", "code": "TV"}, 
            {"name": "Uganda", "code": "UG"}, 
            {"name": "Ukraine", "code": "UA"}, 
            {"name": "United Arab Emirates", "code": "AE"}, 
            {"name": "United Kingdom", "code": "GB"}, 
            {"name": "United States", "code": "US"}, 
            {"name": "United States Minor Outlying Islands", "code": "UM"}, 
            {"name": "Uruguay", "code": "UY"}, 
            {"name": "Uzbekistan", "code": "UZ"}, 
            {"name": "Vanuatu", "code": "VU"}, 
            {"name": "Venezuela", "code": "VE"}, 
            {"name": "Viet Nam", "code": "VN"}, 
            {"name": "Virgin Islands, British", "code": "VG"}, 
            {"name": "Virgin Islands, U.S.", "code": "VI"}, 
            {"name": "Wallis and Futuna", "code": "WF"}, 
            {"name": "Western Sahara", "code": "EH"}, 
            {"name": "Yemen", "code": "YE"}, 
            {"name": "Zambia", "code": "ZM"}, 
            {"name": "Zimbabwe", "code": "ZW"} 
        ]

        this._client = this._formBuilder.group({
            client: this._clientdata,
            services: this._formBuilder.array([this.services]),
            contents: this._formBuilder.array([this.contents]),
            kpi: this._formBuilder.array([this.kpis]),
        })
    }1

    getSelectComponents()
    {
        this._httpClient.get(`${environment.apiUrl}app/list_services/`, this.httpHeaders).subscribe((list_services: any) => {
            const list_services_arr = []
            for (let key in list_services) {
                list_services_arr[key] = {
                    label: list_services[key].service,
                    id: list_services[key].id
                }
            }
            this.service_list = list_services_arr
        })

        this._httpClient.get(`${environment.apiUrl}app/list_contents/`, this.httpHeaders).subscribe((list_contents: any) => {
            const list_contents_arr = []
            for (let key in list_contents) {
                list_contents_arr[key] = {
                    label: list_contents[key].content,
                    id: list_contents[key].id
                }
            }
            this.content_list = list_contents_arr
        })

        this._httpClient.get(`${environment.apiUrl}app/list_kpi/`, this.httpHeaders).subscribe((list_kpi: any) => {
            const list_kpi_arr = []
            for (let key in list_kpi) {
                list_kpi_arr[key] = {
                    label: list_kpi[key].kpi,
                    id: list_kpi[key].id
                }
            }
            this.kpi = list_kpi_arr
        })

        this._httpClient.get(`${environment.apiUrl}app/list_duration/`, this.httpHeaders).subscribe((list_duration: any) => {
            const list_duration_arr = []
            for (let key in list_duration) {
                list_duration_arr[key] = {
                    label: list_duration[key].duration,
                    id: list_duration[key].duration_months
                }
            }
            this.duration = list_duration_arr
        })

        this._httpClient.get(`${environment.apiUrl}app/list_users/`, this.httpHeaders).subscribe((users: any) => {
            var list_pms_arr = []
            var list_writers_arr = []
            for (let key in users) {
                if(users[key].department.id === 3) {
                    var pm = {
                        label: users[key].user.first_name + ' ' + users[key].user.last_name,
                        id: users[key].user.id
                    }

                    list_pms_arr.push(pm)
                }
                
                if(users[key].department.id === 7) {
                    var writer = {
                        label: users[key].user.first_name + ' ' + users[key].user.last_name,
                        id: users[key].user.id
                    }
                    
                    list_writers_arr.push(writer)
                }
            }

            this.pms = list_pms_arr
            this.writers = list_writers_arr
        })

        this._httpClient.get(`${environment.apiUrl}app/list_industry/`, this.httpHeaders).subscribe((list_industry: any) => {
            const list_industry_arr = []
            for (let key in list_industry) {
                list_industry_arr[key] = {
                    label: list_industry[key].industry,
                    id: list_industry[key].id
                }
            }
            this.industries = list_industry_arr
        })

    }

    onChangedStartDate(date: any)
    {
        const dateVal = date.value
        const startmonth = (dateVal.getMonth() + 1) < 10 ? '0' + (dateVal.getMonth() + 1) : (dateVal.getMonth() + 1)
        const startday = dateVal.getDate() < 10 ? '0' + dateVal.getDate() : dateVal.getDate()
        this.startdate = dateVal.getFullYear() + '-' + startmonth + '-' + startday

        const duration = this._duration + 1
        dateVal.setMonth(dateVal.getMonth()+duration)
        const month = dateVal.getMonth() < 10 ? '0' + dateVal.getMonth() : dateVal.getMonth()
        const day = dateVal.getDate() < 10 ? '0' + dateVal.getDate() : dateVal.getDate()
        this.enddate = dateVal.getFullYear() + '-' + month + '-' + day
    }

    onChangedServiceType(service)
    {
        switch(service.value)
        {
            case 1:
                this.service_type = 'sem'
                break
            case 2:
                this.service_type = 'seo'
                break
            case 3:
                this.service_type = 'social'
                break
        }
         
    }

    // onChangedDept(dept)
    // {
    //     this._httpClient.get('http://localhost:8000/app/list_designations/?department=' + dept.value, this.httpHeaders).subscribe((response: any) => {
    //         const list_designations = response.designations
    //         const list_designations_arr = []
    //         for (let key in list_designations) {
    //             list_designations_arr[key] = {
    //                 label: list_designations[key].designation,
    //                 id: list_designations[key].id
    //             }
    //             // Use `key` and `value`
    //         }
    //         this.designations = list_designations_arr
    //     })
    // }
    
    get client(): FormGroup {
        return this._formBuilder.group({
            xero_id: "",
            client_name: "",
            created:"",
            pm: "",
            writer: "",
            duration: "",
            start_date: "",
            end_date: "",
            datecreated: "",
            industry: "",
            other_revenue: "",
            media_fees: "",
            contract: "",
            source: "",
            company_size: ""
        })
    }

    get services(): FormGroup {
        return this._formBuilder.group({
            service: "",
            service_qty: ""
        });
    }
    
    addService() {
        (this._client.get("services") as FormArray).push(this.services);
    }

    deleteService(index) {
        (this._client.get("services") as FormArray).removeAt(index);
    }

    get contents(): FormGroup {
        return this._formBuilder.group({
            content: "",
            content_qty: "",
        });
    }
    
    addContent() {
        (this._client.get("contents") as FormArray).push(this.contents);
    }

    deleteContent(index) {
        (this._client.get("contents") as FormArray).removeAt(index);
    }

    get kpis(): FormGroup {
        return this._formBuilder.group({
            kpi: ""
        });
    }
    
    addKpi() {
        (this._client.get("kpi") as FormArray).push(this.kpis);
    }

    deleteKpi(index) {
        (this._client.get("kpi") as FormArray).removeAt(index);
    }

    createClientForm(): FormGroup
    {
        return this._formBuilder.group({
            client: this._formBuilder.group([this.client]),
            services: this._formBuilder.group([this.services]),
            contents: this._formBuilder.group([this.contents]),
            kpi: this._formBuilder.group([this.kpis]),
        })
    }

}
