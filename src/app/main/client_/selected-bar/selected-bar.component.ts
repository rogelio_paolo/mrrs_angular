import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';

import { HeroService } from 'app/main/hero/hero.service';

@Component({
    selector   : 'selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class ClientSelectedBarComponent implements OnInit, OnDestroy
{
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;
    hasSelectedHero: boolean;
    isIndeterminate: boolean;
    selectedHeroes: string[];

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {ContactsService} _heroService
     * @param {MatDialog} _matDialog
     */
    constructor(
        private _heroService: HeroService,
        public _matDialog: MatDialog
    )
    {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        this._heroService.onSelectedHeroesChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(selectedHeroes => {
                this.selectedHeroes = selectedHeroes;
                setTimeout(() => {
                    this.hasSelectedHero = selectedHeroes.length > 0;
                    this.isIndeterminate = (selectedHeroes.length !== this._heroService.heroes.length && selectedHeroes.length > 0);
                }, 0);
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Select all
     */
    selectAll(): void
    {
        this._heroService.selectHeroes();
    }

    /**
     * Deselect all
     */
    deselectAll(): void
    {
        this._heroService.deselectHeroes();
    }

    /**
     * Delete selected contacts
     */
    deletedSelectedHeroes(): void
    {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected heroes?';

        this.confirmDialogRef.afterClosed()
            .subscribe(result => {
                if ( result )
                {
                    this._heroService.deleteSelectedHeroes();
                }
                this.confirmDialogRef = null;
            });
    }
}
