import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

import { FuseUtils } from '@fuse/utils';

import { Client } from 'app/main/client_/client.model';
import { ClientManager } from 'app/main/client_/clientmanager.model';

import { environment } from 'environments/environment';

@Injectable()
export class ClientService implements Resolve<any>
{
    onClientsChanged: BehaviorSubject<any>;
    onSelectedClientsChanged: BehaviorSubject<any>;
    onUserDataChanged: BehaviorSubject<any>;
    onSearchTextChanged: Subject<any>;
    onFilterChanged: Subject<any>;

    clients: Client[];
    client: ClientManager[];
    user: any;
    selectedClients: string[] = [];

    searchText: string;
    filterBy: string;

    private httpOptions;
    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    )
    {
        const token = localStorage.getItem('authorization')
        this.httpOptions = {
            headers: new HttpHeaders({'Content-Type': 'application/json', 'Authorization': 'token '+token})
        }

        // Set the defaults
        this.onClientsChanged = new BehaviorSubject([]);
        this.onSelectedClientsChanged = new BehaviorSubject([]);
        this.onUserDataChanged = new BehaviorSubject([]);
        this.onSearchTextChanged = new Subject();
        this.onFilterChanged = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getClients(),
                this.getUserData()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getClients();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getClients();
                    });

                    resolve();

                },
                reject
            );
        });
    }

    /**
     * Get contacts
     *
     * @returns {Promise<any>}
     */
    getClients(): Promise<any>
    {
        return new Promise((resolve, reject) => {
                this._httpClient.get(`${environment.apiUrl}app/list_clients/`,this.httpOptions)
                    .subscribe((response: any) => {

                        this.clients = response;
                        console.log(this.clients)

                        if ( this.filterBy === 'starred' )
                        {
                            this.clients = this.clients.filter(_client => {
                                return this.user.starred.includes(_client.id);
                            });
                        }

                        if ( this.filterBy === 'frequent' )
                        {
                            this.clients = this.clients.filter(_client => {
                                return this.user.frequentContacts.includes(_client.id);
                            });
                        }

                        if ( this.searchText && this.searchText !== '' )
                        {
                            this.clients = FuseUtils.filterArrayByString(this.clients, this.searchText);
                        }

                        this.clients = this.clients.map(client => {
                            return new Client(client);
                        });

                        this.onClientsChanged.next(this.clients);
                        resolve(this.clients);
                    }, reject);
            }
        );
    }

    /**
     * Get user data
     *
     * @returns {Promise<any>}
     */
    getUserData(): Promise<any>
    {
        return new Promise((resolve, reject) => {
                this._httpClient.get('api/contacts-user/5725a6802d10e277a0f35724')
                    .subscribe((response: any) => {
                        this.user = response;
                        this.onUserDataChanged.next(this.user);
                        resolve(this.user);
                    }, reject);
            }
        );
    }

    /**
     * Toggle selected contact by id
     *
     * @param id
     */
    toggleSelectedClient(id): void
    {
        // First, check if we already have that contact as selected...
        if ( this.selectedClients.length > 0 )
        {
            const index = this.selectedClients.indexOf(id);

            if ( index !== -1 )
            {
                this.selectedClients.splice(index, 1);

                // Trigger the next event
                this.onSelectedClientsChanged.next(this.selectedClients);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedClients.push(id);

        // Trigger the next event
        this.onSelectedClientsChanged.next(this.selectedClients);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll(): void
    {
        if ( this.selectedClients.length > 0 )
        {
            this.deselectClients();
        }
        else
        {
            this.selectClients();
        }
    }

    /**
     * Select contacts
     *
     * @param filterParameter
     * @param filterValue
     */
    selectClients(filterParameter?, filterValue?): void
    {
        this.selectedClients = [];

        // If there is no filter, select all contacts
        if ( filterParameter === undefined || filterValue === undefined )
        {
            this.selectedClients = [];
            this.clients.map(client => {
                this.selectedClients.push(client.id);
            });
        }

        // Trigger the next event
        this.onSelectedClientsChanged.next(this.selectedClients);
    }

    /**
     * Update contact
     *
     * @param contact
     * @returns {Promise<any>}
     */
    createClient(client): Promise<any>
    {   
        this.client = client
        console.log(this.client)
        return new Promise((resolve, reject) => {
        this._httpClient.post(`${environment.apiUrl}app/clients/`, {...client},this.httpOptions)
            .subscribe(response => {
                this.getClients()
                resolve(response)
            })
        })
    }

    updateClient(client): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._httpClient.patch(`${environment.apiUrl}app/clients/` + client.id + '/', {...client},this.httpOptions)
                .subscribe(response => {
                    this.getClients()
                    resolve(response)
                })
        })
    }

    /**
     * Update user data
     *
     * @param userData
     * @returns {Promise<any>}
     */
    updateUserData(userData): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._httpClient.post('api/contacts-user/' + this.user.id, {...userData})
                .subscribe(response => {
                    this.getUserData();
                    this.getClients();
                    resolve(response);
                });
        });
    }

    /**
     * Deselect contacts
     */
    deselectClients(): void
    {
        this.selectedClients = [];

        // Trigger the next event
        this.onSelectedClientsChanged.next(this.selectedClients);
    }

    /**
     * Delete hero
     *
     * @param hero
     */
    deleteClient(client): Promise<any>
    {
        // const heroIndex = this.heroes.indexOf(hero);
        // this.heroes.splice(heroIndex, 1);
        // this.onClientsChanged.next(this.heroes);

        return new Promise((resolve, reject) => {
            this._httpClient.delete('http://localhost:8000/app/clients/' + client.id + '/',this.httpOptions)
                .subscribe(response => {
                    this.getClients()
                    resolve(response)
                })
        })
    }

    /**
     * Delete selected contacts
     */
    deleteselectedClients(): void
    {
        for ( const clientId of this.selectedClients )
        {
            const client = this.clients.find(_client => {
                return _client.id === clientId;
            });
            const clientIndex = this.clients.indexOf(client);
            this.clients.splice(clientIndex, 1);
        }
        this.onClientsChanged.next(this.clients);
        this.deselectClients();
    }

}
