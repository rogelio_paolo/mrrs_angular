import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatRippleModule } from '@angular/material/core';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { AgmCoreModule } from '@agm/core';
import { MatMenuModule} from '@angular/material/menu';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatToolbarModule } from '@angular/material';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule, MatNativeDateModule } from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';
import { FuseWidgetModule } from '@fuse/components/widget/widget.module';

import { ClientListComponent } from 'app/main/client_/list/list.component';
import { ClientComponent } from 'app/main/client_/client.component';
import { ClientService } from 'app/main/client_/client.service';
import { ClientSelectedBarComponent } from 'app/main/client_/selected-bar/selected-bar.component';
import { ClientMainSidebarComponent } from 'app/main/client_/sidebars/main/main.component';
import { ClientFormDialogComponent } from 'app/main/client_/form/form.component';
import { ClientInnerComponent } from 'app/main/client_/inner/inner.component';
import { ClientInnerService } from 'app/main/client_/inner/inner.service';

const routes: Routes = [
    {
        path     : 'client/list',
        component: ClientComponent,
        resolve  : {
            data: ClientService
        }
    },
    {
        path     : 'client/:id',
        component: ClientInnerComponent,
        resolve  : {
            data: ClientInnerService
        }
    },
    // {
    //     path     : 'client/profile/:id',
    //     component: ClientInnerComponent,
    //     resolve  : {
    //         data: ClientInnerService
    //     }
    // }
];

@NgModule({
    declarations: [
        ClientComponent,
        ClientListComponent,
        ClientSelectedBarComponent,
        ClientMainSidebarComponent,
        ClientFormDialogComponent,
        ClientInnerComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatChipsModule,
        MatExpansionModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatPaginatorModule,
        MatRippleModule,
        MatSelectModule,
        MatSortModule,
        MatSnackBarModule,
        MatTableModule,
        MatTabsModule,
        MatMenuModule,
        MatToolbarModule,
        FormsModule,
        ReactiveFormsModule,
        MatDialogModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatNativeDateModule,
        
        NgxChartsModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
        }),

        FuseSharedModule,
        FuseWidgetModule
    ],
    providers   : [
        ClientService,
        ClientInnerService
    ],
    entryComponents: [ClientFormDialogComponent]
})
export class ClientModule
{
}
