import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { Location } from '@angular/common';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';

import { Client } from 'app/main/client_/client.model';
import { ClientService } from 'app/main/client_/client.service';
import { ClientInnerService } from 'app/main/client_/inner/inner.service';
import { InvoiceListComponent } from 'app/main/invoice/invoice.component';
import { Invoices } from 'app/main/client_/inner/invoices.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'environments/environment';

@Component({
    selector     : 'client-inner',
    templateUrl  : './inner.component.html',
    styleUrls    : ['./inner.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ClientInnerComponent implements OnInit, OnDestroy
{
    client: Client;
    pageType: string;
    clientForm: FormGroup;
    invoices: any;
    httpOptions: { headers: any; };
    lifetimevalue: any;
    owedinvoices: any;
    revenueowed: any;
    clientlife: any;

    private _unsubscribeAll: Subject<any>;

    inv_fil_val = 1
    /**
     * Constructor
     *
     * @param {ClientService} _ClientService
     * @param {FormBuilder} _formBuilder
     * @param {Location} _location
     * @param {MatSnackBar} _matSnackBar
     */
    constructor(
        private _ClientService: ClientService,
        private _ClientInnerService: ClientInnerService,
        private _formBuilder: FormBuilder,
        private _location: Location,
        private _matSnackBar: MatSnackBar,
        private _http: HttpClient
    )
    {
        this._unsubscribeAll = new Subject();

        const token = localStorage.getItem('authorization')
        this.httpOptions = {
            headers: new HttpHeaders({'Content-Type': 'application/json', 'Authorization': 'token '+token})
        }
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        // Subscribe to update product on changes
        this._ClientInnerService.onClientChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(client => {

                this.client = new Client(client)
                this.pageType = 'edit'

                // this.clientForm = this._formBuilder.group({
                //     xeroid: this.client.xeroid,
                //     client: this.client.client,
                //     services: this._formBuilder.array([this.services]),
                //     pm: this.client.pm,
                //     contents: this._formBuilder.array([this.contents]),
                //     writer: this.client.writer,
                //     kpi: this.client.kpi,
                //     duration: this.client.duration,
                //     industry: this.client.industry,
                //     startdate: this.client.startdate,
                //     enddate: this.client.enddate,
                //     datecreated: this.client.datecreated
                // })

                this._http.get(`${environment.apiUrl}app/list_invoices/?xero_id=` + this.client.xeroid, this.httpOptions)
                .subscribe((resp: any) => {
                    // const data = Object.keys(resp.xero_data).map(k => resp.xero_data[k])
                    // const xerodata = {
                    //     curr_mrr: resp.xero_data.SubTotal,
                    //     paid: resp.xero_data.FullyPaidOnDate ? 'yes' : 'no',
                    //     services: resp.xero_data.LineItems
                    // }
                    const data = resp.xero_data
                    this.invoices = data

                    var ltv = 0
                    var owed = 0
                    var revenue = 0
                    
                    data.forEach(function (value) {
                        ltv += value.SubTotal
                        owed += (value.Payments).length > 0 ? 0 : 1
                        revenue += (value.Payments).length > 0 ? 0 : value.SubTotal
                    });

                    this.lifetimevalue = ltv
                    this.owedinvoices = owed
                    this.revenueowed = revenue

                    const oneDay = 24*60*60*1000;
                    const firstDate = new Date()
                    const secondDate = new Date(parseInt((this.client.enddate).substr(0,4)), parseInt((this.client.enddate).substr(5,2)) - 1, parseInt((this.client.enddate).substr(8,2)))
                    const diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)))
                    this.clientlife = diffDays

                    console.log(this.invoices)
                })
            })
        
        
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    monthDiff(d1, d2) {
        console.log(d1, d2)
        var months;
        months = (d2.getFullYear() - d1.getFullYear()) * 12;
        months -= d1.getDays();
        months += d2.getDays();
        return months <= 0 ? 0 : months;
    }

    // saveProduct(): void
    // {
    //     const data = this.productForm.getRawValue();
    //     data.handle = FuseUtils.handleize(data.name);

    //     this._ClientService.saveProduct(data)
    //         .then(() => {

    //             // Trigger the subscription with new data
    //             this._ClientService.onClientsChanged.next(data);

    //             // Show the success message
    //             this._matSnackBar.open('Product saved', 'OK', {
    //                 verticalPosition: 'top',
    //                 duration        : 2000
    //             });
    //         });
    // }

    // get services(): FormGroup {
    //     (this.clientForm.get("services") as FormArray).push(this.client.services)
        
    //     return this._formBuilder.group({
    //         service: "",
    //         service_qty: ""
    //     });
    // }
    
    // addService() {
    //     (this.clientForm.get("services") as FormArray).push(this.services);
    // }

    // deleteService(index) {
    //     (this.clientForm.get("services") as FormArray).removeAt(index);
    // }

    // get contents(): FormGroup {
    //     (this.clientForm.get("contents") as FormArray).push(this.client.contents)

    //     return this._formBuilder.group({
    //         content: "",
    //         content_qty: "",
    //     });
    // }
    
    // addContent() {
    //     (this.clientForm.get("contents") as FormArray).push(this.contents);
    // }

    // deleteContent(index) {
    //     (this.clientForm.get("contents") as FormArray).removeAt(index);
    // }

}
