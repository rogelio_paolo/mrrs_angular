import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router'
import { BehaviorSubject, Observable } from 'rxjs'
import { environment } from 'environments/environment';

@Injectable()
export class ClientInnerService implements Resolve<any>
{
    routeParams: any;
    client: any;
    onClientChanged: BehaviorSubject<any>;
    httpOptions: { headers: any; };
    onInvoicesChanged: BehaviorSubject<any>;
    xero_invoices: any;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    )
    {
        const token = localStorage.getItem('authorization')
        this.httpOptions = {
            headers: new HttpHeaders({'Content-Type': 'application/json', 'Authorization': 'token '+token})
        }
        
        this.onClientChanged = new BehaviorSubject({})
        this.onInvoicesChanged = new BehaviorSubject({})
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
        this.routeParams = route.params;

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getClient()
            ]).then(
                () => {
                    resolve();
                },
                reject
            )
        });
    }

    getClient(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            
            this._httpClient.get(`${environment.apiUrl}app/list_clients/` + this.routeParams.id, this.httpOptions)
                .subscribe((response: any) => {
                    this.client = response;
                    this.onClientChanged.next(this.client);
                    resolve(response);
                }, reject)
            
        })
    }

    saveClient(client): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._httpClient.patch(`${environment.apiUrl}app/list_clients/` + client.id + '/', client, this.httpOptions)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

}
