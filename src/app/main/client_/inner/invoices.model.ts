import { FuseUtils } from '@fuse/utils';
export class Invoices

{
    invoiceid: string
    client: String
    services: any
    paid: Number

    /**
     * 
     * Constructor
     *
     * @param contact
     */
    constructor(invoice?)
    {
        {
            this.invoiceid = invoice.InvoiceID || ""
            this.client = invoice.Contact.Name || ""
            this.services = invoice.LineItems || ""
            this.paid = invoice.Payments || ""
        }
    }
}
