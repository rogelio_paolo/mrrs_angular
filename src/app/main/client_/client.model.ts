import { FuseUtils } from '@fuse/utils';
export class Client

{
    id: string
    xeroid: string
    client: String
    services: any
    pm:any
    contents: any
    writer: any
    kpi: any
    duration: any
    industry: any
    startdate: String
    enddate: String
    datecreated: String

    /**
     * 
     * Constructor
     *
     * @param contact
     */
    constructor(client?)
    {
        {
            this.id = client.id || ""
            this.xeroid = client.xero_id || ""
            this.client = client.client_name || ""
            this.services = client.services || ""
            this.pm = client.pm || ""
            this.contents = client.contents || ""
            this.writer = client.writer || ""
            this.kpi = client.kpi || ""
            this.duration = client.duration || ""
            this.industry = client.industry || ""
            this.startdate = client.start_date || ""
            this.enddate = client.end_date || ""
            this.datecreated = client.created_at || ""
        }
    }
}
