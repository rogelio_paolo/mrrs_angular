export const locale = {
    lang: 'en',
    data: {
        'NAV': {
            'APPLICATIONS': '',
            'DASHBOARD'        : {
                'TITLE': 'Dashboard',
                'BADGE': false
            },
            'NOTIFICATION'        : {
                'TITLE': 'Notifications',
                'BADGE': false
            },
            'CLIENTLIST'        : {
                'TITLE': 'Clients',
                'BADGE': false
            },
            'CSMCLIENTLIST'     : {
                'TITLE': 'CSM Clients',
                'BADGE': false
            },
            'CONTENTLIST'        : {
                'TITLE': 'Contents',
                'BADGE': false
            },
            'INVOICELIST'        : {
                'TITLE': 'Invoices',
                'BADGE': false
            },
            'HEROLIST'        : {
                'TITLE': 'Heroes',
                'BADGE': false
            },
            'CALENDAR'        : {
                'TITLE': 'Calendar',
                'BADGE': false
            },
            'AUTH': 'Auth',
            'LOGIN'        : {
                'TITLE': 'Login',
                'BADGE': false
            },
            'FORGOTPASSWORD'        : {
                'TITLE': 'Forgot Password',
                'BADGE': false
            }
        }
    }
};
