import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
    {
        id       : 'applications',
        title    : '',
        translate: 'NAV.APPLICATIONS',
        type     : 'group',
        children : [
            {
                id       : 'dashboard',
                title    : 'Dashboard',
                translate: 'NAV.DASHBOARD.TITLE',
                type     : 'item',
                icon     : 'dashboard',
                url      : '/dashboard'
            },
            {
                id       : 'notification-list',
                title    : 'Notifications',
                translate: 'NAV.NOTIFICATION.TITLE',
                type     : 'item',
                icon     : 'notifications',
                url      : '/notification/list'
            },
            {
                id       : 'client-list',
                title    : 'Clients',
                translate: 'NAV.CLIENTLIST.TITLE',
                type     : 'item',
                icon     : 'recent_actors',
                url      : '/client/list'
            },
            {
                id       : 'csmclient-list',
                title    : 'CSM Clients',
                translate: 'NAV.CSMCLIENTLIST.TITLE',
                type     : 'item',
                icon     : 'recent_actors',
                url      : '/csmclient/list'
            },
            {
                id       : 'content-list',
                title    : 'Contents',
                translate: 'NAV.CONTENTLIST.TITLE',
                type     : 'item',
                icon     : 'book',
                url      : '/content/list'
            },
            {
                id       : 'invoice-list',
                title    : 'Invoices',
                translate: 'NAV.INVOICELIST.TITLE',
                type     : 'item',
                icon     : 'table_chart',
                url      : '/invoice/list'
            },
            {
                id       : 'hero-list',
                title    : 'Heroes',
                translate: 'NAV.HEROLIST.TITLE',
                type     : 'item',
                icon     : 'group',
                url      : '/hero/list'
            },
            {
                id       : 'calendar',
                title    : 'Calendar',
                translate: 'NAV.CALENDAR.TITLE',
                type     : 'item',
                icon     : 'calendar_today',
                url      : '/calendar'
            }
        ]   
    },
    // {   
    //     id       : 'client',
    //     title    : 'Client',
    //     translate: 'NAV.CLIENT',
    //     type     : 'group',
    //     children : [
    //         {
    //             id       : 'client-list',
    //             title    : 'List',
    //             translate: 'NAV.CLIENTLIST.TITLE',
    //             type     : 'item',
    //             icon     : 'recent_actors',
    //             url      : '/client/list'
    //         },
    //         {
    //             id       : 'client-inner',
    //             title    : 'Inner',
    //             translate: 'NAV.CLIENTINNER.TITLE',
    //             type     : 'item',
    //             icon     : 'record_voice_over',
    //             url      : '/client/inner'
    //         }
    //     ]
    // },
    // {
    //     id       : 'auth',
    //     title    : 'Auth',
    //     translate: 'NAV.AUTH',
    //     type     : 'group',
    //     children : [
    //         {
    //             id       : 'login',
    //             title    : 'Login',
    //             translate: 'NAV.LOGIN.TITLE',
    //             type     : 'item',
    //             icon     : 'email',
    //             url      : '/auth/login'
    //         },
    //         {
    //             id       : 'forgot-password',
    //             title    : 'Forgot Password',
    //             translate: 'NAV.FORGOTPASSWORD.TITLE',
    //             type     : 'item',
    //             icon     : 'vpn_key',
    //             url      : '/auth/forgot-password'
    //         }
    //     ]
    // }
];
